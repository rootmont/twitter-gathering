# twittermont

INSTALL USING DOCKER

FROM python:3.7

RUN mkdir /twitter-ms
COPY ./requirements.txt /twitter-ms
WORKDIR /twitter-ms
RUN pip install -r requirements.txt

ENV env=database_dev

CMD ["python", "main.py"]
