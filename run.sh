#!/bin/bash

REBUILD=1;

function main {
    process_args $@;
    if [[ ${REBUILD} -eq 0 ]]; then
        echo "Building without cache.";
        docker-compose build --no-cache --force-rm --parallel;
        docker-compose up --force-recreate --renew-anon-volumes;
    else
        docker-compose up --build
    fi
}

function process_args {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -h|--help)
                echo "$0 [options]"
                echo "    options:"
                echo "        -h, --help                Show help."
                echo "        -r,                       Rebuild image completely, discarding cache. To be used when updating dependencies."
                exit 0
                ;;
            -r)
                shift
                REBUILD=0;
                ;;
            *)
                break
                ;;
        esac
    done
}

main $@;