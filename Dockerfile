FROM python:3.7

COPY ./common /common
COPY ./twitter-ms /twitter-ms
COPY ./requirements.txt /twitter-ms
WORKDIR /twitter-ms
RUN pip install -r requirements.txt

CMD ["python", "main.py"]
