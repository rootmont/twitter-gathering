
from twitter_app.flask_factory import app
from twitter_app.router import configure_api

configure_api(app)


if __name__ == '__main__':

    app.run(host="0.0.0.0")
