from sshtunnel import SSHTunnelForwarder
from time import sleep

with SSHTunnelForwarder(
    ('13.59.11.11', 22),
    ssh_username="anthony",
    ssh_password="hianthony",
    remote_bind_address=('twitterapi.cluster-cshpiwdqubcf.us-east-2.rds.amazonaws.com', 3306),
    local_bind_address=('0.0.0.0', 3306),
    ssh_private_key='/Users/anthonymunoz/Documents/twittermont/twitter-ms/anthony-robert.pem'

) as server:

    print(server.local_bind_port)
    while True:
        # press Ctrl-C for stopping
        sleep(1)
        
print('FINISH!')