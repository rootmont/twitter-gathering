# coding: utf-8

import logging
import json
import time
import datetime

#from zappa.async import task

from flask_restplus import Resource
from twitter_app.rest.api import api
from twitter_app.rest.twitter.schedule import *

from twitter_app.app.twitter_module import (
    twitterModule
)

log = logging.getLogger(__name__)


ns = api.namespace('twitter', description='twitter service')

@ns.route('/twitter_general_information')
class twitterService(Resource):
    def get(self):
        twitter_instance = twitterModule()
        return twitter_instance.request()