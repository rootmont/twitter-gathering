from flask import Flask
from flask_dotenv import DotEnv
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import pymysql
pymysql.install_as_MySQLdb()

from twitter_app.app.config import connection_string


app = Flask(__name__)

env = DotEnv()
env.init_app(app)

app.config['SQLALCHEMY_DATABASE_URI'] = connection_string
app.config['SECRET_KEY'] = 'supersecret'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_POOL_SIZE'] = 10
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 30
app.config['SQLALCHEMY_POOL_RECYCLE'] = 60
app.config['SQLALCHEMY_MAX_OVERFLOW'] = 5

settings  = app.config

db = SQLAlchemy(app)

migrate = Migrate(app, db)


@app.cli.command()
def twitter_run_pullings():

    from twitter_app.app.twitter_module import twitterModule

    twitterModule().request()

    return True


    