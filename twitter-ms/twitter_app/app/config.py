from configparser import SafeConfigParser
import os

# config = SafeConfigParser()
# config.read('config.ini')

DBSTRING='{4}://{2}:{3}@{0}/{1}?charset=utf8mb4'
API_KEY = "OWZlYzUyNzYwODg1MWNhNjZmZTZjZmE5ZjgxMTZmMjg6YTVhOTRhNjM5MmE2OWJmOTY3NGUzOTJiMjlmZmQwNzk="

# API_KEY = config.get('twitter', 'API_KEY')

# ENV = config.get('twitter', 'env')
# ENV = 'database_dev'
# if os.environ['env'] is not None:
#     ENV = os.environ['env']

# print(ENV)
#
# if ENV in ["localdb", "localqa", "localstage", "localprod", "docker", "database", "database_dev"]:
#
#     DBTYPE = config.get(ENV, 'DBTYPE')
#     DBDRIVER = config.get(ENV, 'DBDRIVER')
#     DBHOST = config.get(ENV, 'DBHOST')
#     DBNAME = config.get(ENV, 'DBNAME')
#     DBUSER = config.get(ENV, 'DBUSER')
#     DBPASS = config.get(ENV, 'DBPASS')
#     # DBSTRING = config.get(ENV, 'DBSTRING').replace('->', '=').replace(',,', ';')
#
# if ENV in ["QA", "STAGE", "PROD"]:
#
#     DBTYPE = os.environ['DBTYPE']
#     DBDRIVER = os.environ['DBDRIVER']
#     DBHOST =  os.environ['DBHOST']
#     DBNAME = os.environ['DBNAME']
#     DBUSER = os.environ['DBUSER']
#     DBPASS = os.environ['DBPASS']
#     # DBSTRING = os.environ['DBSTRING'].replace('->', '=').replace(',,', ';')
#     DEBUG = 'False'

DBTYPE = os.environ.get('DBTYPE') or 'mysql'
DBDRIVER = os.environ.get('DBDRIVER')
DBHOST =  os.environ['DBHOST']
# DBNAME = os.environ['DBNAME']
DBDATABASE = os.environ['DBDATABASE'] or 'api'
DBUSER = os.environ['DBUSER']
DBPASS = os.environ['DBPASS']
# DBSTRING = os.environ['DBSTRING'].replace('->', '=').replace(',,', ';')
DEBUG = 'False'
num_retries = 20
sleep_time_seconds = 10
PRODUCTIONENV = os.environ.get('PRODUCTIONENV', False)

if PRODUCTIONENV:
    num_processes = 10
else:
    num_processes = 1

# connection_string = DBSTRING.format(DBHOST, DBNAME, DBUSER, DBPASS, DBTYPE, DBDRIVER)
connection_string = DBSTRING.format(DBHOST, DBDATABASE, DBUSER, DBPASS, DBTYPE, DBDRIVER)
