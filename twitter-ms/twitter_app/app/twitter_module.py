from __future__ import print_function
import time
import logging
import twitter as twitter_api
from twython import Twython
import tweepy
import pandas as pd
from common.config import format_logger, maybe_parallel
from twitter_app.app.config import sleep_time_seconds, num_retries, num_processes
from twitter_app.app.utils import Timer
from twitter_app.flask_factory import db
from twitter_app.models.models import TwitterMetrics

logger = logging.getLogger('twitter')
format_logger(logger)


############################################ COMPANIES LIST API ###############################################

consumer_key='dO2MDv4LmVZQCkXlTDuvC1Vlf'
consumer_secret='vtdN4SemMWIgGKuHyweXsGBzl4mY5cIa6kYybBIr09ZHhc3qzJ'
access_token_key='1110975440276082688-yNUp3UtVIWVxfYtm68NgEyveXqF1vK'
access_token_secret='TXrNj3cp1AFI5dEYtpnRzMNxV82rr7C2CeuWtRgJXG8Tc'

t = twitter_api.Api(consumer_key=consumer_key,
                      consumer_secret=consumer_secret,
                      access_token_key=access_token_key,
                      access_token_secret=access_token_secret)

t_twython = Twython(app_key=consumer_key, 
            app_secret=consumer_secret, 
            oauth_token=access_token_key, 
            oauth_token_secret=access_token_secret)

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token_key, access_token_secret)
api_tweepy = tweepy.API(auth,wait_on_rate_limit=True)


class twitterModule():

    def database_parsing_and_saving(self, object_, coin_data, source_id):

        preview = TwitterMetrics.query.filter_by(tweet_id=object_["tweet_id"]).all()
        
        if preview:

            logger.info("{}:Duplication tweet ID was detected, ID: {}".format(coin_data.get("TICKER"), object_["tweet_id"]))

        else:
            
            InstanceTwitterMetrics = TwitterMetrics(
                likes = object_["likes"],
                replies = object_["replies"],
                retweets = object_["retweets"],
                screen_name = object_["screen_name"],
                text = object_["text"],
                tweet_created_at = object_["tweet_created_at"],
                tweet_id = object_["tweet_id"],
                user = object_["user"],

                ticker = coin_data.get("TICKER"),
                coin = coin_data.get("Name"),
                company = coin_data.get("Twitter"),
                ticker_dolar= coin_data.get("TICKER_$"),

                source_id = source_id)

            db.session.merge(InstanceTwitterMetrics)

            db.session.commit()

        return True

    def request_coin(self, coin):
        if len(coin) < 2:
            logger.warning('Unable to process coin {}'.format(coin))
            return
        logger.info("STAGE 0 - COIN: {} ".format(coin[1].get("TICKER")))
        try:

            results = get_data(coin[1])

            for i, result in enumerate(results):
                for item in result:

                        logger.info("STAGE {} - COIN: {} ITEM: {}".format(i+1, coin[1].get("TICKER"), item))

                        data_with_metrics = single_inspection(item)
                        database_status = self.database_parsing_and_saving(data_with_metrics, coin[1], i+1)
                        logger.info("database status: {}".format(database_status))

        except Exception as error:

            logger.error(error)

    def request(self, next_page=None):

        response_time = Timer()

        payload = {"result":True}

        try:
            
            logger.info("API is starting 2.0")

            dataframe = get_information_for_iteration()

            payload['data'] = maybe_parallel(fxn=self.request_coin, args=dataframe.iterrows(), num_processes=num_processes, parallel=True)


        except Exception as e:

            logger.info("Exception when calling TweetsAPI->get_information_for_iteration: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time_seconds)

            return payload
                
        return payload



def add_dolar_symbol(x):
                            
    ticker = x.get("TICKER")
                            
    ticker_ = "$"+str(ticker)
                            
    return ticker_
    

def get_information_for_iteration():

    search_table = pd.read_excel('Twitter Search Names.xlsx', index_col=0)

    search_table = search_table.reset_index()

    search_table["TICKER_$"] = search_table.apply(add_dolar_symbol, axis=1)
    
    return search_table


def get_user_timeline(screen_name, max_id=None, collection=[]):
    
    global collection_
    
    try:
        
        user_timeline = t_twython.get_user_timeline(screen_name=screen_name, count=100, max_id=max_id)
        
    except Exception as e:
        
        logger.info(e)
        
        return collection

    collection_ = collection + user_timeline

    logger.info("Recursively searching... S:{}-E:{}-{}".format(user_timeline[0]["created_at"],user_timeline[-1]["created_at"],len(collection_)))

    if user_timeline and max_id == user_timeline[-1]["id"]:
        
        logger.info("Recursively process has ended... TOTAL ELEMENTS: {}".format(len(collection_)))
        
        return collection_
            
    else:
        
        get_user_timeline(screen_name, max_id=user_timeline[-1]["id"],collection=collection_)
    
    return collection_



def single_inspection(collection_):
    
    tweet = collection_
    
    output = {}
        
    if isinstance(collection_, twitter_api.models.Status):
        
        metrics = get_metrics_by_tweet_id_and_user(tweet.user.screen_name, tweet.id)

        output["tweet_created_at"] = tweet.created_at
        output["tweet_id"] = tweet.id
        output["replies"] = metrics[0]["replies"]
        output["retweets"] = metrics[1]["retweets"]
        output["likes"] = metrics[2]["likes"]
        output["screen_name"] = tweet.user.screen_name
        output["user"] = tweet.user.name
        output["text"] = tweet.text
        
    else:

        metrics = get_metrics_by_tweet_id_and_user(tweet["user"]["screen_name"], tweet["id"])

        if "retweeted_status" in tweet:

            logger.info(tweet["retweeted_status"])

        else:

            logger.info("No retweeted status")

        output["tweet_created_at"] = tweet["created_at"]
        output["tweet_id"] = tweet["id"]
        output["replies"] = metrics[0]["replies"]
        output["retweets"] = metrics[1]["retweets"]
        output["likes"] = metrics[2]["likes"]
        output["screen_name"] = tweet["user"]["screen_name"]
        output["user"] = tweet["user"]["name"]
        output["text"] = tweet["text"]

    return output

def get_metrics_by_tweet_id_and_user(user, tweet_id):

    import requests
    
    from bs4 import BeautifulSoup

    headers = {'Accept-Language': 'en-US,en;q=0.8'}

    company_or_page = user
    
    html = requests.get('https://twitter.com/{}/status/{}'.format(company_or_page,tweet_id), headers=headers)
    
    soup = BeautifulSoup(html.text, 'lxml')

    metrics = []
    
    for element in soup.find_all('span', attrs={'class':'ProfileTweet-actionCountForAria'})[0:3]:

        metrics_element_json = {}
        
        value = element.contents[0]
        
        if value.split()[1] == "retweet":
        
            metrics_element_json["retweets"] = value.split()[0]
            
        elif value.split()[1] == "like":
        
            metrics_element_json["likes"] = value.split()[0]
                        
        elif value.split()[1] == "reply":
        
            metrics_element_json["replies"] = value.split()[0]
        
        else:
            
            metrics_element_json[value.split()[1]] = value.split()[0]
            
        
        metrics.append(metrics_element_json)
                                                            
    return metrics
    


def tweets_by_ticker(ticker, max_id=None, collection=[]):
    success = False
    for i in range(num_retries):
        try:
            replies = t.GetSearch(raw_query="q=%23{}&src=typd".format(ticker), count=100, since_id=None, max_id=None)
            success = True
            break
        except twitter_api.TwitterError as e:
            logger.warning('Received TwitterError while making request for {}: {}'.format(ticker, e))
            logger.warning('Sleeping to wait it out')
            time.sleep(sleep_time_seconds * (2 ** i))
            continue
    if not success:
        raise Exception('Max retries exceeded for {}'.format(ticker))

    if len(replies) == 0:
        logger.info("No Replies Found")
        return collection


    logger.info("Recursively searching... ")
    
    collection_ = collection + replies
    
    if replies:
        logger.info("TOTAL Replies for {}: {} - Date: {}".format(ticker, len(replies), replies[-1].created_at))

        if max_id == replies[-1].id:

            logger.info("Recursively process has ended... TOTAL ELEMENTS: {}".format(len(collection)))

            return collection

        else:

            collection_ = collection + replies

            tweets_by_ticker(ticker, max_id=replies[-1].id,collection=collection_)
        
    return collection_


def tweets_by_dolar_ticker(ticker, max_id=None, collection=[]):
    success = False
    for i in range(num_retries):
        try:
            replies = t.GetSearch(raw_query="q=%23%24{}&src=typd".format(ticker), count=100, since_id=None, max_id=None)
            success = True
            break
        except twitter_api.TwitterError as e:
            logger.warning('Received TwitterError while making request for {}: {}'.format(ticker, e))
            logger.warning('Sleeping to wait it out')
            time.sleep(sleep_time_seconds * (2 ** i))
            continue
    if not success:
        raise Exception('Max retries exceeded for {}'.format(ticker))


    logger.info("Recursively searching for tweets for {}... ".format(ticker))
    
    collection_ = collection + replies

    if replies:
        logger.info("TOTAL Replies for {}: {} - Date: {}".format(ticker, len(replies), replies[-1].created_at))

        if max_id == replies[-1].id:

            logger.info("Recursively process has ended... TOTAL ELEMENTS: {}".format(len(collection)))

            return collection

        else:

            tweets_by_dolar_ticker(ticker, max_id=replies[-1].id,collection=collection_)
        
    return collection_


def tweets_by_coin_name(coin_name, max_id=None, collection=[]):
    success = False
    for i in range(num_retries):
        try:
            replies = t.GetSearch(raw_query="q=%23{}&src=typd".format(coin_name), count=100, since_id=None, max_id=None)
            success = True
            break
        except twitter_api.TwitterError as e:
            logger.warning('Received TwitterError while making request for {}: {}'.format(coin_name, e))
            logger.warning('Sleeping to wait it out')
            time.sleep(sleep_time_seconds * (2 ** i))
            continue
    if not success:
        raise Exception('Max retries exceeded for {}'.format(coin_name))

    logger.info("Recursively searching for tweets for {}... ".format(coin_name))
    
    collection_ = collection + replies
                
    if replies:
    
        if max_id == replies[-1].id:

            logger.info("Recursive process has ended... TOTAL ELEMENTS: {}".format(len(collection)))

            return collection

        else:

            collection_ = collection + replies

            tweets_by_coin_name(coin_name, max_id=replies[-1].id,collection=collection_)        
        
    return collection_

def tweets_directed_at_the_company_using_the_hashtag(company_or_coin):
    
    replies = t.GetSearch(raw_query="q=%23{}&src=typd".format(company_or_coin), count=100, since_id=None, max_id=None)
    
    #logger.info("TOTAL Replies: {}".format(len(replies)))
    
    for reply in replies:
        
        logger.info("ID: {}, RID: {}, name: {} DT: {}".format(reply.id,reply.in_reply_to_status_id, reply.user.name, reply.created_at))

    return True


def tweets_directed_at_the_company_using_the_hashtag(company_or_coin):
    
    replies = t.GetSearch(raw_query="q=%23{}&src=typd".format(company_or_coin), count=100, since_id=None, max_id=None)
    
    #logger.info("TOTAL Replies: {}".format(len(replies)))
    
    for reply in replies:
        
        logger.info("ID: {}, RID: {}, name: {} DT: {}".format(reply.id,reply.in_reply_to_status_id, reply.user.name, reply.created_at))

    return True


def get_data(x):
        
    ticker = x.get("TICKER")
    name = x.get("Name")
    account = x.get("Twitter")
    ticker_dolar= x.get("TICKER_$")[1:-1]

    logger.info("get_user_timeline({})...".format(account))

    result1 = get_user_timeline(account)

    logger.info("{}: Getting tweets_by_ticker...".format(ticker))

    result2 = tweets_by_ticker(ticker)

    logger.info("{}: Getting tweets_by_dollar_ticker...".format(ticker_dolar))

    result3 = tweets_by_dolar_ticker(ticker_dolar)

    logger.info("{}: Getting tweets_by_coin_name...".format(name))

    result4 = tweets_by_coin_name(name)
    
    logger.info("{}: ·················· ".format(ticker))

    return result1, result2, result3, result4


    