import twitter as twitter_api

consumer_key=None
consumer_secret=None
access_token_key=None
access_token_secret=None

t = twitter_api.Api(consumer_key=consumer_key,
                      consumer_secret=consumer_secret,
                      access_token_key=access_token_key,
                      access_token_secret=access_token_secret)

                      

def tweets_by_ticker(ticker, max_id=None, collection=[]):
    
    replies = t.GetSearch(raw_query="q=%23{}&src=typd".format(ticker), count=100, since_id=None, max_id=None)
    
    print("TOTAL Replies: {} - Date: {}".format(len(replies), replies[-1].created_at))
        
    print("Recursively searching... ")
    
    collection_ = collection + replies
    
    if replies:

        if max_id == replies[-1].id:

            print("Recursively process has ended... TOTAL ELEMENTS: {}".format(len(collection)))

            return collection

        else:

            collection_ = collection + replies

            tweets_by_ticker(ticker, max_id=replies[-1].id,collection=collection_)
        
    return collection_