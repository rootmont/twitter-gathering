from twitter_app.flask_factory import db

# coding: utf-8
from sqlalchemy.dialects.mysql import INTEGER, TINYINT, LONGTEXT, SMALLINT, TIMESTAMP, BIGINT

from sqlalchemy.ext.declarative import declarative_base

from twitter_app.models.mixins import TimeStampsMixin, SoftDeletesMixin


Base = declarative_base()
metadata = Base.metadata

class TwitterMetrics(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'twitter'

    id = db.Column(db.Integer, primary_key=True)

    ticker_dolar = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    company = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    coin = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    
    likes = db.Column(INTEGER, nullable=True)
    replies = db.Column(INTEGER, nullable=True)
    retweets = db.Column(INTEGER, nullable=True)
    screen_name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    user = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    tweet_created_at = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    tweet_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    text = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    source_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
