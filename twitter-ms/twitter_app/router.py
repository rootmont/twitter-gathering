import logging
from flask import Blueprint
from flask_cors import CORS
from twitter_app.rest.api import api
from twitter_app.rest.twitter.api import ns as twitter_namespace


log = logging.getLogger(__name__)


def _configure_namespaces(api):
	"""
		Add more namespaces HERE
	"""
	#twitter_namespace
	api.add_namespace(twitter_namespace)


def configure_api(flask_app):
	blueprint = Blueprint('api', __name__)
	api.init_app(blueprint)
	_configure_namespaces(api)
	CORS(flask_app)
	flask_app.register_blueprint(blueprint)
