cd /home/anthony/twittermont/twitter-ms
source /home/anthony/twittermont/twitter-ms/env/bin/activate
export env=database
pwd
echo "Starting twitter thread"

if [[ ! $(pgrep -f main.py) ]]; then
    echo "No running"
    python3.6 main.py
else
    echo "Running"
fi
