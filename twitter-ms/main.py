from twitter_app.app.twitter_module import twitterModule
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, Response
import datetime

app = Flask(__name__)
scheduler = BackgroundScheduler()


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running:
        return Response("{}", status=500, mimetype='application/json')
    return Response("{}", status=200, mimetype='application/json')


now = datetime.datetime.now()

if __name__ == '__main__':
    scheduler.add_job(
        func=lambda: twitterModule().request(),
        start_date=now + datetime.timedelta(minutes=1),
        trigger='interval',
        hours=24,
        max_instances=1,
        replace_existing=False
    )
    scheduler.start()
    app.run(host='0.0.0.0', debug=True, port=5000)



