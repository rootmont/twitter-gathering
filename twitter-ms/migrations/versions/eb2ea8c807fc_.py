"""empty message

Revision ID: eb2ea8c807fc
Revises: 
Create Date: 2019-03-30 10:26:08.132998

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'eb2ea8c807fc'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('tweet_analytics',
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('deleted_at', sa.DateTime(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('ticker_dolar', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('ticker', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('company', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('coin', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('likes', mysql.INTEGER(), nullable=True),
    sa.Column('replies', mysql.INTEGER(), nullable=True),
    sa.Column('retweets', mysql.INTEGER(), nullable=True),
    sa.Column('screen_name', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('tweet_created_at', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('tweet_id', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_tweet_analytics_coin'), 'tweet_analytics', ['coin'], unique=False)
    op.create_index(op.f('ix_tweet_analytics_company'), 'tweet_analytics', ['company'], unique=False)
    op.create_index(op.f('ix_tweet_analytics_screen_name'), 'tweet_analytics', ['screen_name'], unique=False)
    op.create_index(op.f('ix_tweet_analytics_ticker'), 'tweet_analytics', ['ticker'], unique=False)
    op.create_index(op.f('ix_tweet_analytics_ticker_dolar'), 'tweet_analytics', ['ticker_dolar'], unique=False)
    op.create_index(op.f('ix_tweet_analytics_tweet_created_at'), 'tweet_analytics', ['tweet_created_at'], unique=False)
    op.create_index(op.f('ix_tweet_analytics_tweet_id'), 'tweet_analytics', ['tweet_id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_tweet_analytics_tweet_id'), table_name='tweet_analytics')
    op.drop_index(op.f('ix_tweet_analytics_tweet_created_at'), table_name='tweet_analytics')
    op.drop_index(op.f('ix_tweet_analytics_ticker_dolar'), table_name='tweet_analytics')
    op.drop_index(op.f('ix_tweet_analytics_ticker'), table_name='tweet_analytics')
    op.drop_index(op.f('ix_tweet_analytics_screen_name'), table_name='tweet_analytics')
    op.drop_index(op.f('ix_tweet_analytics_company'), table_name='tweet_analytics')
    op.drop_index(op.f('ix_tweet_analytics_coin'), table_name='tweet_analytics')
    op.drop_table('tweet_analytics')
    # ### end Alembic commands ###
